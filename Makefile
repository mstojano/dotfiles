all: bash fonts git vim programs zsh
minimal: bash git vim

bash:
	./scripts/aliases.sh
	./scripts/bash.sh

fonts:
	./scripts/fonts.sh

git:
	./scripts/git.sh

programs:
	./scripts/programs.sh

vim:
	./scripts/vim.sh

zsh:
	./scripts/aliases.sh
	./scripts/zsh.sh

update:
	./scripts/update.sh

clean:
	./scripts/clean.sh
