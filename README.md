It's a mess... Just run `make all` and hope it works :)
One day maybe I'll write a decent README

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

RTFM, or in this case the installation scripts in [scripts](scripts).
Installs and configures:

- aliases and exports
- bash and bashrc
- bat
- fd-find
- fonts needed for zsh, vim, neovim
- git template hooks
- Gogh: dracula and gruvbox-dark profiles for GNU Terminal
- neovim and config file
- ripgrep
- tigrc and config
- tmux and tmux.conf (uses submodules)
- tmuxinator and config
- vifm (with vifmimg, no support for epub and fonts preview)
- vim and vimrc (with other vim setups and fzf)
- zsh (uses submodules) and zshrc

## Install
Clone repo:
```sh
git clone --recursive git@gitlab.com:mstojano/dotfiles.git
```

Enter repo and run:
```sh
make all
```

Main `make` targets:

| Target | Configs installed |
| ------ | ------ |
| all | bash fonts git nvim programs zsh |
| minimal | bash git vim |


`minimal` does not need `sudo` permissions, `all` does.

Cleanup target is available but only does a minor cleanup of linked added files and dirs.

## License
MIT
