set encoding=utf-8
scriptencoding=utf-8
let using_neovim = has('nvim')
let using_vim = !using_neovim

" Use same dirs for both vim and neovim
" https://neovim.io/doc/user/nvim.html#nvim-from-vim
if using_neovim
  set runtimepath^=~/.vim runtimepath+=~/.vim/after
  let &packpath = &runtimepath
endif

""" Vim-Plug
if using_vim
  " Automatically install and run if not found
  let g:vim_home = $HOME
  let $MYVIMRC = g:vim_home . '/.vimrc'
  if empty(glob(g:vim_home . '/.vim/autoload/plug.vim'))
    exec 'silent !curl -fLo ' . g:vim_home . '/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    augroup vimPlug
      autocmd!
      autocmd VimEnter * PlugInstall | source $MYVIMRC
    augroup END
  endif
endif

" :PlugUpgrade to upgrade VIM-PLUG
" :PlugUpdate to update plugins
" :PlugClean! to remove plugins that aren't on the list
call plug#begin()

"" Aesthetics
" Themes
Plug 'dracula/vim'
Plug 'morhetz/gruvbox'

" Make stuff look pretty
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'junegunn/vim-journal'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"" Functionalities
Plug 'airblade/vim-gitgutter' " shows a git diff in the sign column
Plug 'alvan/vim-closetag' " auto close tags form html xml
Plug 'dkarter/bullets.vim' " automated bullet lists
Plug 'jiangmiao/auto-pairs' " Insert or delete brackets, parentheses, quotes in pair
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " fuzzy finder
Plug 'junegunn/fzf.vim' " fuzzy finder vim plugin
Plug 'mbbill/undotree' " keep undo history after save
Plug 'mhinz/vim-startify' " start screen
Plug 'scrooloose/nerdtree' " file tree
Plug 'scrooloose/nerdcommenter' " easy to comment out lines
Plug 'sheerun/vim-polyglot' " collection of language packs for Vim
Plug 'tpope/vim-fugitive' " git support
Plug 'airblade/vim-rooter' " allow fzf to look from root dir of git repo not from current dir
Plug 'tpope/vim-surround' " easy manage brackets and tags

call plug#end()

if using_neovim
  " Python3 VirtualEnv
  let g:python3_host_prog = expand('~/.vim/env/bin/python')

  " vim-pydocstring
  let g:pydocstring_doq_path = '~/.vim/env/bin/doq'
endif

""" Coloring

syntax on
augroup setColor
  if using_neovim
    autocmd vimenter * ++nested color gruvbox
  else
    autocmd vimenter * let g:airline_theme='gruvbox'
    autocmd vimenter * color gruvbox
  endif
  set bg=dark
  " transparent background
  autocmd vimenter * hi Normal guibg=NONE ctermbg=NONE
augroup END
highlight Pmenu guibg=white guifg=black gui=bold
highlight Comment gui=bold
highlight Normal gui=NONE
highlight NonText guibg=NONE

" Opaque Background (Comment out to use terminal's profile)
if using_neovim
  set termguicolors
endif

""" Other Configurations

"" Set datadir
let g:target_dir='~/.cache/vim_mydatadir/'
let data_dir=expand(target_dir)
let g:backup_dir = g:data_dir . 'backup'
let g:swap_dir = g:data_dir . 'swap'
let g:undo_dir = g:data_dir . 'undofile'
let g:conf_dir = g:data_dir . 'conf'
if finddir(g:data_dir) ==# ''
silent call mkdir(g:data_dir, 'p', 0700)
endif
if finddir(g:backup_dir) ==# ''
silent call mkdir(g:backup_dir, 'p', 0700)
endif
if finddir(g:swap_dir) ==# ''
silent call mkdir(g:swap_dir, 'p', 0700)
endif
if finddir(g:undo_dir) ==# ''
silent call mkdir(g:undo_dir, 'p', 0700)
endif
if finddir(g:conf_dir) ==# ''
silent call mkdir(g:conf_dir, 'p', 0700)
endif
let &undodir = g:undo_dir
let &backupdir = g:backup_dir
let &directory = g:swap_dir
unlet g:data_dir
unlet g:backup_dir
unlet g:swap_dir
unlet g:undo_dir
unlet g:conf_dir

"" Setup
filetype plugin indent on
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smarttab autoindent
set incsearch ignorecase smartcase hlsearch
set ruler laststatus=2 showcmd showmode
set list listchars=trail:»,tab:»-
set fillchars+=vert:\
if using_neovim
  set wrap breakindent
endif
set number
set relativenumber
set title
set scrolloff=3
set sidescrolloff=5
set display+=lastline
set splitright
set splitbelow
set mouse=
set backup
set nowritebackup
set spell spelllang=en_us
set clipboard=unnamedplus " use system clipboard
set ttimeout        " time out for key codes
set ttimeoutlen=0 " wait up to 0ms after ESC for special key
if has('persistent_undo')
  set undofile " Used by undotree plugin
  set undolevels=100
endif
" Smart j/k (move by display lines unless a count is provided)
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
" Reselect visual selection after indenting
vnoremap < <gv
vnoremap > >gv

""" Plugin Configurations
"" Airline
let g:airline_powerline_fonts = 1
let g:airline_section_warning = ''
let g:airline#extensions#tabline#enabled = 1

"" Bullets
" created pull request to fix checkbox https://github.com/dkarter/bullets.vim/pull/101
let g:bullets_enabled_file_types = ['gitcommit', 'journal', 'markdown', 'text']
let g:bullets_checkbox_markers = ' .:oOX'
" created pull request for this https://github.com/dkarter/bullets.vim/pull/98
let g:bullets_outline_levels = ['std*', 'std-', 'std*', 'std>', 'std#', 'std+']

"" Deoplete
let g:deoplete#enable_at_startup = 1
" Disable documentation window
set completeopt-=preview

"" fzf-vim
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'Type'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Character'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
let g:fzf_layout = { 'down': '40%' }
let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline'

"" indentLine
let g:indentLine_char = '▏'
let g:indentLine_color_gui = '#363949'

"" NERDTree
let NERDTreeShowHidden=1
let g:NERDTreeDirArrowExpandable = '↠'
let g:NERDTreeDirArrowCollapsible = '↡'

"" vim-gutgetter
set updatetime=100

"" Filetype-Specific Configurations
augroup setFileType
  " HTML, XML, Jinja
  autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
  autocmd FileType css setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
  autocmd FileType xml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
  autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
  autocmd FileType htmldjango inoremap {{ {{  }}<left><left><left>
  autocmd FileType htmldjango inoremap {% {%  %}<left><left><left>
  autocmd FileType htmldjango inoremap {# {#  #}<left><left><left>

  " Journal, markdown, rst and vim
  autocmd FileType journal setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
  autocmd FileType markdown setlocal shiftwidth=2 tabstop=2 softtabstop=2 textwidth=80 expandtab
  autocmd FileType rst setlocal shiftwidth=2 tabstop=2 softtabstop=2 textwidth=80 expandtab
  autocmd FileType vim setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
augroup END

""" Custom Functions

"" Trim Whitespaces
function! TrimWhitespace()
  let l:save = winsaveview()
  " the linter doesnt want the %s to be by itself, the correct way would be
  " to use substitute({expr}, {pat}, {sub}, {flags})
  exec '%s/\\\@<!\s\+$//e'
  call winrestview(l:save)
endfunction

"" Dracula Mode (Dark)
function! ColorDracula()
  let g:airline_theme=''
  color dracula
  IndentLinesEnable
  " transparent background
  hi Normal guibg=NONE ctermbg=NONE
endfunction

"" GruveBox Mode (Dark)
function! ColorGruvBox()
  let g:airline_theme='gruvbox'
  color gruvbox
  set bg=dark
  IndentLinesEnable
  " transparent background
  hi Normal guibg=NONE ctermbg=NONE
endfunction

""" Custom Mappings
nmap <leader>q :NERDTreeToggle<CR>
nmap \ <leader>q
nmap <leader>u :UndotreeToggle<CR>
nmap <leader>ee :Colors<CR>
nmap <leader>ea :AirlineTheme
nmap <leader>e1 :call ColorDracula()<CR>
nmap <leader>e2 :call ColorGruvBox()<CR>
nmap <leader>t :call TrimWhitespace()<CR>
nmap <leader>d <Plug>(pydocstring)
nmap <leader>f :Files<CR>
nmap <leader>h :RainbowParentheses!!<CR>
nmap <leader>j :set filetype=journal<CR>
nmap <silent> <leader><leader> :noh<CR>
if using_neovim
  augroup setNeovim
    " Neovim :Terminal
    autocmd BufWinEnter,WinEnter term://* startinsert
    autocmd BufLeave term://* stopinsert
    autocmd FileType python nmap <leader>T :0,$!~/.vim/env/bin/python -m yapf<CR>
  augroup END
  nmap <leader>r :so ~/.vimrc<CR>
  nmap <leader>s <C-w>s<C-w>j:terminal<CR>
  nmap <leader>vs <C-w>v<C-w>l:terminal<CR>
endif
