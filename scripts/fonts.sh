#!/bin/bash -e

# Install NERD fonts for icons and a beautiful airline bar (https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts) (I'll be using Hack for Powerline)
echo "[***] Downloading patch font into ""$HOME""/.local/share/fonts/NerdFonts ..."
# Just to make sure we do have curl
sudo apt update
sudo apt install curl -y
mkdir -p "$HOME"/.local/share/fonts/NerdFonts
cd "$HOME"/.local/share/fonts/NerdFonts
curl -fLO https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Hack/Regular/HackNerdFontMono-Regular.ttf
curl -fLO https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Hack/Italic/HackNerdFontMono-Italic.ttf
curl -fLO https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Hack/Bold/HackNerdFontMono-Bold.ttf
curl -fLO https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Hack/BoldItalic/HackNerdFontMono-BoldItalic.ttf
fc-cache -f "$HOME"/.local/share/fonts/NerdFonts
echo "[+++] NERD fonts installed"
