#!/bin/bash -e

# Set .bashrc
echo "[**] Set bash config ..."
if [ -L "$HOME/.bashrc" ]; then
    echo "[*] Unlink old .bashrc ..."
    unlink "$HOME"/.bashrc
elif [ -f "$HOME/.bashrc" ]; then
    echo "[*] Move old .bashrc to .bashrc_back ..."
    mv "$HOME"/.bashrc "$HOME"/bash_back
    echo "[+] Moved old config to bashrc_back"
fi

echo "[*] Create sym link for .bashrc ..."
ln -sv "$PWD"/configs/bashrc "$HOME"/.bashrc
echo "[+] New .bashrc is set"

echo "[+++] Bash setup completed! Set bash as your default shell if needed to use it!"
echo " "
