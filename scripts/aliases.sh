#!/bin/bash -e

# Fix $HOME/.aliases file to have correct config for all the needed stuff
echo "[*] Setting up aliasis file ..."
if [ -f "$HOME/.aliases" ]
then
    cp "$HOME"/.aliases "$HOME"/aliases_back
    echo "[+] Moved old .aliases to aliases_back"
fi
cat "$PWD"/aliases >> "$HOME"/.aliases
sed -i "/export DOTFILES/c\export DOTFILES=${PWD}" "$HOME"/.aliases
awk -i inplace '!seen[$0]++' "$HOME"/.aliases
chmod 700 "$HOME"/.aliases
echo "[+] Populated .aliases"

echo "[!!!] PLEASE FILL EMPTY ENTRIES IN ""$HOME""/.aliases [***]"
