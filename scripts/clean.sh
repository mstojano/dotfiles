#!/bin/bash -e

echo "[***] Clean MS configs"
# Aliases
echo "[*] aliases"
if [ -f "$HOME"/.aliases ]
then
    rm "$HOME"/.aliases
    if [ -f "$HOME"/aliases_back ]
    then
        mv "$HOME"/aliases_back "$HOME"/.aliases
    fi
fi
echo "[+] .aliases cleaned"

# Bash
echo "[*] bash"
if [ -L "$HOME"/.bashrc ]
then
    unlink "$HOME"/.bashrc
    if [ -f "$HOME"/bashrc_back ]
    then
        mv "$HOME"/bashrc_back "$HOME"/.bashrc
    fi
fi
echo "[+] .bashrc cleaned"

# fd-find
echo "[*] fd"
if [ -L "$HOME/bin/fd" ]; then
    unlink "$HOME"/bin/fd
fi
echo "[*] fd cleand"

# fzf
echo "[*] fzf"
if [ -L "$HOME/bin/fzf" ]
then
    unlink "$HOME"/bin/fzf
fi
if [ -L "$HOME/bin/fzf-tmux" ]; then
    unlink "$HOME"/bin/fzf-tmux
fi
echo "[+] fzf cleaned"

# git
echo "[*] git"
if [ -L "$HOME"/.git_template ]
then
    unlink "$HOME"/.git_template
    if [ -d "$HOME"/git_template_back ]
    then
        mv "$HOME"/git_template_back "$HOME"/.git_template
    fi
fi
echo "[+] .git_template cleaned"

# NVIM
echo "[*] NVim"
if [ -d "$HOME"/.config/nvim ]
then
    rm -rf "$HOME"/.config/nvim
    if [ -d "$HOME"/.config/nvim_back ]
    then
        mv "$HOME"/config/nvim_back "$HOME"/.config/nvim
    fi
fi
if [ -d "$HOME/.vim" ]
then
    rm -rf "$HOME"/.vim
    if [ -d "$HOME"/vim_back ]
    then
        mv "$HOME"/vim_back "$HOME"/.vim
    fi
fi
echo "[+] NVim cleaned"

# tig
echo "[*] Tig"
if [ -L "$HOME"/.tigrc ]
then
    unlink "$HOME"/.tigrc
    if [ -f "$HOME"/tigrc_back ]
    then
        mv "$HOME"/tigrc_back "$HOME"/.tigrc
    fi
fi
echo "[+] .tigrc cleaned"

# tmux
echo "[*] Tmux"
if [ -L "$HOME"/.tmux.conf ]
then
    unlink "$HOME"/.tmux.conf
    if [ -f "$HOME"/tmux.conf_back ]
    then
        mv "$HOME"/tmux.conf_back "$HOME"/.tmux.conf
    fi
fi
echo "[+] .tmux.conf cleaned"

# vim
echo "[*] Vim"
if [ -d "$HOME/.vim" ]
then
    rm -rf "$HOME"/.vim
    if [ -d "$HOME"/vim_back ]
    then
        mv "$HOME"/vim_back "$HOME"/.vim
    fi
fi
if [ -L "$HOME"/.vimrc ]
then
    unlink "$HOME"/.vimrc
    if [ -f "$HOME"/vimrc_back ]
    then
        mv "$HOME"/vimrc_back "$HOME"/.vimrc
    fi
fi
echo "[+] vim cleaned"

# zsh
echo "[*] zsh"
if [ -f "$HOME"/.zshrc ]
then
    rm "$HOME"/.zshrc
    if [ -f "$HOME"/zshrc_back ]
    then
        mv "$HOME"/zshrc_back "$HOME"/.zshrc
    fi
fi
echo "[+] .zshrc cleaned"

echo "[!!!] This script does not remove fonts or other installed programs!"
echo "[!!!] Check installation scripts for installed programs"
echo "[+++] MS configs cleaned"
