#!/bin/bash -e

echo "[**] Set .git_tamplate ..."
if [ -L "$HOME/.git_template" ]; then
    echo "[*] Unlink old .git_template ..."
    unlink "$HOME"/.git_template
elif [ -d "$HOME/.git_template" ]; then
    mv "$HOME"/.git_template "$HOME"/git_template_back
    echo "[*] Move old .git_template to git_template_back ..."
fi

echo "[*] Create .git_template link ..."
ln -sv "$PWD"/configs/git_template "$HOME"/.git_template
echo "[+] New .git_template is set"

# add entry to .gitconfig
echo "[init]" >> "$HOME"/.gitconfig
echo "    templatedir = ~/.git_template" >> "$HOME"/.gitconfig
echo "[!!!] Check your ~/.gitconfing for new changes!!!"

echo "[+++] .git_template setup completed!"
echo " "
