#!/bin/bash -e

# Install zsh if for any reason its not on the system
echo "[***] Setting up zsh ..."
echo "[*] Install zsh to system ..."
sudo apt update
sudo apt install zsh thefuck -y

# Set .zshrc
echo "[**] Set zsh config ..."
if [ -L "$HOME/.zshrc" ]
then
    echo "[[*] Unlink old .zshrc ..."
    unlink "$HOME"/.zshrc
elif [ -f "$HOME/.zshrc" ]
then
    mv "$HOME"/.zshrc "$HOME"/zsh_back
    echo "[+] Old config saved to zshrc_back"
fi
echo "[*] Create sym link for .zshrc ..."
ln -sv "$PWD"/configs/zshrc "$HOME"/.zshrc
echo "[+] New .zshrc is set"

echo "[+++] zsh setup completed! Set zsh as your default shell if needed to use it!"
echo "[!!!] zsh requires NERD fonts!"
echo " "
