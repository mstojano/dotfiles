#!/bin/bash -e

echo "[**] Set vim config ..."
if [ -L "$HOME/.vim" ]; then
    echo "[*] Unlink old .vim ..."
    unlink "$HOME"/.vim
    echo "[*] Create .vim dir ..."
elif [ -d "$HOME/.vim" ] && [ ! -f "$HOME/.vim/msflag" ]; then
    echo "[*] Move old .vim to .vim_back ..."
    mv "$HOME"/.vim "$HOME"/vim_back
    echo "[*] Move old .vim to vim_back ..."
fi

echo "[*] Create .vim dir ..."
mkdir -p "$HOME"/.vim
touch "$HOME"/.vim/msflag
echo "[+] New .vim is set"

if [ -L "$HOME/.vimrc" ]; then
    echo "[*] Unlink old .vimrc ..."
    unlink "$HOME"/.vimrc
elif [ -f "$HOME/.vimrc" ]; then
    echo "[*] Move old .vimrc to .vimrc_back ..."
    mv "$HOME"/.vimrc "$HOME"/.vimrc_back
fi

# Run vim to install plugins, managed by vimrc
echo "[*] Install plugins to vim ..."
cp configs/config.vim "$HOME"/.vimrc
vim -c ':PlugInstall' -c ':UpdateRemotePlugins' -c ':qall'
rm -rf "$HOME"/.vimrc

# Link fzf to be available system wide
if [ ! -d "$HOME/bin" ]; then
    mkdir "$HOME"/bin
elif [ -L "$HOME/bin/fzf" ]; then
    unlink "$HOME"/bin/fzf
    echo "[+] Unlink fzf"
elif [ -L "$HOME/bin/fzf-tmux" ]; then
    unlink "$HOME"/bin/fzf-tmux
    echo "[+] Unlink fzf-tmux"
fi

ln -sv "$HOME"/.vim/plugged/fzf/bin/fzf "$HOME"/bin/fzf
ln -sv "$HOME"/.vim/plugged/fzf/bin/fzf-tmux "$HOME"/bin/fzf-tmux
echo "[*] fzf link created"

echo "[*] Create sym link for .vimrc ..."
ln -sv "$PWD"/configs/config.vim "$HOME"/.vimrc
echo "[+] New .vimrc is set"

echo "[+++] vim setup completed!"
echo "[!!!] vim requires NERD fonts!"
echo " "
