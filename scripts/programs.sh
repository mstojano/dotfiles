#!/bin/bash -e

## Install
echo "[***] Setting up different additional programs ..."
sudo apt update
# Split into lines to make it easier to follow what basically depends on what
sudo apt install fd-find \
    ripgrep \
    tig \
    tmux \
    pipx \
    -y

echo "[**] programs.sh: gogh ..."
# gogh - GNOME Terminal profiles
sudo apt-get install dconf-cli uuid-runtime -y
git clone https://github.com/Mayccoll/Gogh.git gogh
cd gogh/installs
# necessary on ubuntu
export TERMINAL=gnome-terminal
# install themes
./dracula.sh
./gruvbox-dark.sh
cd ../..
rm -rf gogh

## Configs
# fd
# On ubuntu/debian there is some other executable fd so we need to relink this one
echo "[**] Set fd ..."
if [ ! -d "$HOME/bin" ]; then
    mkdir "$HOME"/bin
    echo "[*] created $HOME/bin ..."
fi
if [ ! -L "$HOME/bin/fd" ]; then
    ln -sv "$(command -v fdfind)" "$HOME"/bin/fd
    echo "[*] Link for fd created ..."
fi
echo "[+++] fd setup completed!"
echo " "

# tig
echo "[**] Set tig config ..."
if [ -L "$HOME/.tigrc" ]; then
    echo "[*] Unlink old .tigrc ..."
    unlink "$HOME"/.tigrc
elif [ -f "$HOME/.tigrc" ]; then
    mv "$HOME"/.tigrc "$HOME"/tigrc_back
    echo "[+] Move old .tigrc to tigrc_back ..."
fi

echo "[*] Create sym link for .tigrc ..."
ln -sv "$PWD"/configs/tigrc "$HOME"/.tigrc
echo "[+] New .tigrc is set"

echo "[+++] tig setup completed!"
echo " "

# tmux
echo "[**] Set tmux config ..."
if [ -L "$HOME/.tmux.conf" ]; then
    echo "[*] Unlink old .tmux.conf ..."
    unlink "$HOME"/.tmux.conf
elif [ -f "$HOME/.tmux.conf" ]; then
    mv "$HOME"/.tmux.conf "$HOME"/tmux.conf_back
    echo "[+] Move old .tmux.conf to tmux.conf_back ..."
fi

echo "[*] Create sym link for .tmux.conf ..."
ln -sv "$PWD"/configs/tmux.conf "$HOME"/.tmux.conf
echo "[+] New .tmux.conf is set"

echo "[+++] tmux setup completed!"
echo " "
