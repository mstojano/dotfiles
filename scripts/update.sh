#!/bin/bash -e

git pull origin master

# vim
vim -c ':PlugUpgrade' -c ':PlugUpdate' -c ':qall'
echo "[*] Update vim"

# Update submodules
# The idea is that if zshrc points to the one in this repo bat was installed
# by the installation script and thus can be upgraded too.
if [ "$(readlink -fe "$HOME/.zshrc")" = "$DOTFILES/configs/zshrc" ]; then
    ## pre-commit
    if type pre-commit &> /dev/null; then
        pre-commit autoupdate
        echo "[*] Update pre-commit"
    fi

    ## submodules
    git submodule foreach git pull origin master
    echo "[*] Update submodules"

    ## commit updates if any
    if ! git status | grep -q "nothing to commit"; then
        git add --all
        git commit -m "run update"
        echo "[*] Changes commited to repo"
        echo "[*] Push changes!"
    fi
fi
